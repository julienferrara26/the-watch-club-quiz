<?php

session_start();

$name = $_SESSION['Name'];

if (empty($_SESSION['Name']))
{
    header('location:erreur403b.php?');;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="questionnaire.css">
    <script src="epic.js" async></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300&display=swap" rel="stylesheet">
    <title>Epic</title>
</head>
<body>

    <header>
        <img src="./The watch.png" alt="logo">
    </header>
    
    <div class="container">
        <div id="quiz">
          <h1><span>W</span>atch Club Quiz</h1>

          <p id=Username><?= $_SESSION['Name'] ?></p>
    
          <h2 id="question"></h2>

          <div id="countdown"></div>
            
          <h3 id="score"></h3>

          <h4 id="lien"></h4>
          
          <div class="choices">
            <button id="guess0" class="btn">        
              <p id="choice0"></p>
            </button>
            
            <button id="guess1" class="btn">
              <p id="choice1"></p>
            </button>
            
            <button id="guess2" class="btn">
              <p id="choice2"></p>
            </button>
            
            <button id="guess3" class="btn">
              <p id="choice3"></p>
            </button>
          </div>

          
          
          <p id="progress"></p>
          <br>
          <form method="post" action="/classement.php">
          <input type="hidden" id="score" name="score">
          </form>

        </div>
    </div>

    <footer>
        <img src="./logo.png" alt="logo-footer">
        <p>Copyright 2021</p>
        <p>Ferrara Julien</p>
    </footer>
    
</body>
</html>