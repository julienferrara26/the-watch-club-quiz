class Question {
    constructor(text, choices, answer) {
      this.text = text;
      this.choices = choices;
      this.answer = answer;
    }
    isCorrectAnswer(choice) {
      return this.answer === choice;
    }
  }

  let questions = [
    new Question("Quel est le premier film à avoir été réalisé en images de synthèse ?", ["La Petite Sirène", "Blanche Neige", "Toy Story", "Ghost In The Shell"], "Toy Story"),
    new Question("Qui double la voix de l'Âne dans “Shrek” en VO ?", ["Bruce Willis","Danny DeVito", "Eddie Murphy", "Jean Reno"], "Eddie Murphy"),
    new Question("De quelle couleur est la moto dans le film “Akira” ?", ["Bleue","Rouge", "Verte", "Jaune"], "Rouge"),
    new Question("De quelle année date “Le Roi Lion” ?", ["1995","1994", "1996", "1997"], "1994"),
    new Question("Comment se nomme la princesse dans “Aladdin” ?", ["Mireille", "Camelia", "Gwendoline", "Jasmine"], "Jasmine"),
    new Question("Quel film n'a PAS été réalisé par Miyazaki ?", ["Princesse Monokoké", "Le Chateau ambulant", "Le Voyage de Chihiro", "Le Royaume des chats"], "Le Royaume des chats"),
    new Question("Quel est le nom du voisin qui horripile Homer Simpson ?", ["Denis La Malice", "Ned Flanders", "Peter Griffin", "Stan Smith"], "Ned Flanders"),
    new Question("Dans “L'étrange Noel de monsieur Jack” celui-ci est le roi des... ?", ["Belettes", "Cons", "Citrouilles", "Monstres"], "Citrouilles"),
    new Question("Quelle série précède “Dragon Ball Z” ?", ["Dragon Ball GT", "Dragon Ball", "Dragon Ball Super", "Dragon Ball XXX"], "Dragon Ball"),
    new Question("Quelle série animée met en scene quatre jeunes garçon au langage quelque peu décalé ?", ["South Park", "Les Simpsons", "Hé Arnold!", "American Dad"], "South Park")
  ];

  class Quiz {
    constructor(questions) {
      this.score = 0;
      this.questions = questions;
      this.currentQuestionIndex = 0;
    }
    getCurrentQuestion() {
      return this.questions[this.currentQuestionIndex];
    }
    guess(answer) {
      if (this.getCurrentQuestion().isCorrectAnswer(answer)) {
        this.score++;
      }
      this.currentQuestionIndex++;
    }
    hasEnded() {
      return this.currentQuestionIndex >= this.questions.length;
    }
  }


////// REGROUPER TOUTES LES FONCTIONS RELATIVES A L'AFFICHAGE (comme ça c'est plus propre tiavuuu)

const display = {
    elementVisible: function(id, text) {
      let element = document.getElementById(id);
      element.innerHTML = text;
    },
    endQuiz: function() {
      endQuizHTML = `
        <h1>Quiz terminé !</h1>
        <h3> Ton score est de : ${quiz.score} / ${quiz.questions.length}</h3>
        <h4><a href="./classement.php?id_theme=1">Ton classement</a></h4>`;
      this.elementVisible("quiz", endQuizHTML);
      },
      score: function() {
        return quiz.score
      },
    question: function() {
      this.elementVisible("question", quiz.getCurrentQuestion().text);
      },
    choices: function() {
      let choices = quiz.getCurrentQuestion().choices;
  
      guessHandler = (id, guess) => {
        document.getElementById(id).onclick = function() {
          quiz.guess(guess);
          quizApp();
        }
      }

          // AFFICHAGE DES CHOIX ET DE LA PROGRESSION

    for(let i = 0; i < choices.length; i++) {
        this.elementVisible("choice" + i, choices[i]);
        guessHandler("guess" + i, choices[i]);
      }
    },
    progress: function() {
      let currentQuestionNumber = quiz.currentQuestionIndex + 1;
      this.elementVisible("progress", "Question " + currentQuestionNumber + " sur " + quiz.questions.length);
      this.elementVisible("score", "Score : " + quiz.score);
    },
  };

  // Logique du jeu

quizApp = () => {
    if (quiz.hasEnded()) {
      display.endQuiz();
      let SCOREDATA = {
        score: display.score(),
        theme: 1
      }
      sendScores(SCOREDATA)
    } else {
      display.question();
      display.choices();
      display.progress();
    } 
  }

  // Creation Quiz

  let quiz = new Quiz(questions);
  quizApp();
  
  console.log(quiz);

  // Compte à rebours

    let startingMinutes = 2; // A Combien de temps on commence

    let time = startingMinutes * 60;
  
    const countDownEl = document.getElementById('countdown');
  
    setInterval(updateCountdown, 1000); // J'appelle la fonction toutes les secondes
  
    function updateCountdown() {
      const minutes = Math.floor(time / 60); 
      let seconds = time % 60;
  
      seconds = seconds < 10 ? '0' + seconds : seconds; // Je concatene un 0
  
      countDownEl.innerHTML = `${minutes} : ${seconds}`; // On affiche et des que le chrono se finit, on arrete le quiz
      if (time > 0) {
      time--; 
      } else {
          display.endQuiz();
          let SCOREDATA = {
            score: display.score(),
        }
          sendScores(SCOREDATA)
      }
    }

  // Envoi score
  
  function sendScores (SCOREDATA) {

    fetch('./uploadScore.php', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/JSON',
      },
      body: JSON.stringify(SCOREDATA),
    })
  }