<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="deconnexion.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300&display=swap" rel="stylesheet">
    <title>Deconnexion</title>
</head>
<body>

    <header>
        <img src="./The watch.png" alt="logo">
    </header>

    <section class="Utilisateur">
        <div id="gif">
        <iframe src="https://giphy.com/embed/12pWOEeKbbfdio" width="480" height="266" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
        </div>
        <p>Tu nous quittes déja ?</p>
        <p>Si c'est une erreur, reviens sur la page des thèmes en cliquant<a href="thema.php"> ici</a></p>
        <p>Ou alors cliques sur le bouton de déconnexion.</p>
        <button><a href="./traitement_deco.php">Deconnexion</a></button>
        <br>
    </section>

    <footer>
        <img src="./logo.png" alt="logo-footer">
        <p>Copyright 2021</p>
        <p>Ferrara Julien</p>
    </footer>
    
</body>
</html>