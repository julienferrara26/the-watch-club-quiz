<?php

session_start();

$name = $_SESSION['Name'];

if (empty($_SESSION['Name']))
{
    header('location:erreur403b.php?');;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="themes.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300&display=swap" rel="stylesheet">
    <title>Themes</title>
</head>
<body>

    <header>
        <img src="./The watch.png" alt="logo">
    </header>

    <h1>The <span>W</span>atch Club</h1>
    <br>
    <br>
    <h2>Coucou <?= $_SESSION['Name'] ?> ! Choisis le thème que tu veux.</h2>
    
    <div class="choices">
        <button id="guess0" class="btn"onclick="window.location='questionnaire.php'">        
          <h3>General</h3>
        </button>
        
        <button id="guess1" class="btn"onclick="window.location='epic.php'">
          <h3>Epic</h3>
        </button>
        
        <button id="guess2" class="btn"onclick="window.location='gangsta.php'">
          <h3>Gangster</h3>
        </button>
        
        <button id="guess3" class="btn"onclick="window.location='anime.php'">
        <h3>Animé</h3>
        </button>

        <button id="guess4" class="btn"onclick="window.location='comedy.php'">
            <h3>Comedie</h3>
        </button>

        <button id="guess5" class="btn"onclick="window.location='science.php'">
            <h3>SF</h3>
        </button>
    </div>

    <button id="Deco"><a href="./deconnexion.php">Deconnexion</a></button>

    <footer>
        <img src="./logo.png" alt="logo-footer">
        <p>Copyright 2021</p>
        <p>Ferrara Julien</p>
    </footer>
    
</body>
</html>