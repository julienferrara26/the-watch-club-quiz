class Question {
    constructor(text, choices, answer) {
      this.text = text;
      this.choices = choices;
      this.answer = answer;
    }
    isCorrectAnswer(choice) {
      return this.answer === choice;
    }
  }

  let questions = [
    new Question("Quelle est la devise de Tony Montana dans le film “Scarface” ?", ["Yippee ki yay", "Hasta la vista", "The world is yours", "Coucou kikou"], "The world is yours"),
    new Question("Le film “American Gangster” retrace la vie du celebre gangster ?", ["Tony Montana", "Franck Lucas", "Al Capone", "Mickey Cohen"], "Franck Lucas"),
    new Question("Quel est le nom du personnage interpreté par Ray Liotta dans le film “Les Affranchis” ?", ["Henry Hill", "Jimmy Conway", "Tommy DeVito", "Paul Cicero"], "Henry Hill"),
    new Question("Quel personnage n'apparait PAS dans “Reservoir Dogs” ?", ["Mr Pink", "Mr White", "Mr Orange", "Mr Red"], "Mr Red"),
    new Question("Le film “Heat” réunit deux icones du cinema, lesquelles ?", ["Eastwood - Fonda", "De Niro - Pacino", "Lellouche - Dujardin", "DiCaprio - Nicholson"], "De Niro - Pacino"),
    new Question("Qui a réalisé “Les Tontons Flingueurs” ?", ["George Lautner", "Michel Audiard", "Albert Simonin", "Jean-Luc Godard"], "George Lautner"),
    new Question("Quelle actrice joue dans “Gangs of New-York” ?", ["Charlize Theron", "Sophie Marceau", "Kerry Washington", "Cameron Diaz"], "Cameron Diaz"),
    new Question("Dans “Le Prophète” de Jacques Audiard, le personnage passe par ?", ["Les Goudes", "La prison", "Un monastère", "Derrière..."], "La prison"),
    new Question("A quelle communauté appartient le personnage de Brad Pitt dans le film “Snatch, tu braques ou tu raques” ?", ["Russe", "Irlandaise", "Gitane", "Allemande"], "Gitane"),
    new Question("Dans “Breaking Bad” comment se nomme la belle-soeur de Walter White ?", ["Skyler", "Mary", "Lydia", "Beth"], "Mary")
  ];

  class Quiz {
    constructor(questions) {
      this.score = 0;
      this.questions = questions;
      this.currentQuestionIndex = 0;
    }
    getCurrentQuestion() {
      return this.questions[this.currentQuestionIndex];
    }
    guess(answer) {
      if (this.getCurrentQuestion().isCorrectAnswer(answer)) {
        this.score++;
      }
      this.currentQuestionIndex++;
    }
    hasEnded() {
      return this.currentQuestionIndex >= this.questions.length;
    }
  }


////// REGROUPER TOUTES LES FONCTIONS RELATIVES A L'AFFICHAGE (comme ça c'est plus propre tiavuuu)

const display = {
    elementVisible: function(id, text) {
      let element = document.getElementById(id);
      element.innerHTML = text;
    },
    endQuiz: function() {
      endQuizHTML = `
        <h1>Quiz terminé !</h1>
        <h3> Ton score est de : ${quiz.score} / ${quiz.questions.length}</h3>
        <h4><a href="./classement.php?id_theme=4">Ton classement</a></h4>`;
      this.elementVisible("quiz", endQuizHTML);
    },
    score: function() {
      return quiz.score
    },
    question: function() {
      this.elementVisible("question", quiz.getCurrentQuestion().text);
    },
    choices: function() {
      let choices = quiz.getCurrentQuestion().choices;
  
      guessHandler = (id, guess) => {
        document.getElementById(id).onclick = function() {
          quiz.guess(guess);
          quizApp();
        }
      }

          // AFFICHAGE DES CHOIX ET DE LA PROGRESSION

    for(let i = 0; i < choices.length; i++) {
        this.elementVisible("choice" + i, choices[i]);
        guessHandler("guess" + i, choices[i]);
      }
    },
    progress: function() {
      let currentQuestionNumber = quiz.currentQuestionIndex + 1;
      this.elementVisible("progress", "Question " + currentQuestionNumber + " sur " + quiz.questions.length);
      this.elementVisible("score", "Score :" + quiz.score);
    },
  };

  // Logique du jeu

quizApp = () => {
    if (quiz.hasEnded()) {
      display.endQuiz();
      let SCOREDATA = {
        score: display.score(),
        theme: 4
      }
      sendScores(SCOREDATA)
    } else {
      display.question();
      display.choices();
      display.progress();
    } 
  }

  // Creation Quiz

  let quiz = new Quiz(questions);
  quizApp();
  
  console.log(quiz);

  // Compte à rebours

    let startingMinutes = 2; // A Combien de temps on commence

    let time = startingMinutes * 60;
  
    const countDownEl = document.getElementById('countdown');
  
    setInterval(updateCountdown, 1000); // J'appelle la fonction toutes les secondes
  
    function updateCountdown() {
      const minutes = Math.floor(time / 60); 
      let seconds = time % 60;
  
      seconds = seconds < 10 ? '0' + seconds : seconds; // Je concatene un 0
  
      countDownEl.innerHTML = `${minutes} : ${seconds}`; // On affiche et des que le chrono se finit, on arrete le quiz
      if (time > 0) {
      time--; 
      } else {
          display.endQuiz();
          let SCOREDATA = {
            score: display.score(),
        }
          sendScores(SCOREDATA)
      }
    }

  // Envoi score

  function sendScores (SCOREDATA) {

    fetch('./uploadScore.php', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/JSON',
      },
      body: JSON.stringify(SCOREDATA),
    })
  }
