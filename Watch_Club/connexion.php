
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="accueil.css">
    <script src="connexion.js" async></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300&display=swap" rel="stylesheet">
    <title>Connexion</title>
</head>
<body>

    <header>
        <img src="./The watch.png" alt="logo">
    </header>

    <section class="Utilisateur">
        <p>Tadaaa ! Tu es désormais membre du club.</p>
        <p>Entres ton joli nom et ton mot de passe et commences à répandre ton savoir !</p>
        <br>
        
        <form action="traitement_co.php" method="POST">
            <div><input type="text" id="name" name="name" placeholder="Nom" /></div>
            <input type="password" name="password" id="password" placeholder="Password"/>
           
            <input type="submit" value="Valider"> 
        </form>
          
        <div id="resultat">
            Nom : <span id="res-name"></span><br />
        </div>

        <a href="vitrine.html">Retour à la page d'accueil</a>
        <br>
    </section>

    <footer>
        <img src="./logo.png" alt="logo-footer">
        <p>Copyright 2021</p>
        <p>Ferrara Julien</p>
    </footer>
    
</body>
</html>