<?php

////Ouvrir la session 

session_start();

$name = $_SESSION['Name'];

if (empty($_SESSION['Name']))
{
    header('location:erreur403b.php?');;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="rules.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300&display=swap" rel="stylesheet">
    <title>Regles</title>
</head>
<body>

    <header>
        <img src="./The watch.png" alt="logo">
    </header>

    <section>
        <p>Bienvenue à toi jeune Padawan, celui ou celle que l'on nomme <span><?= $_SESSION['Name'] ?> </span>! Tu te sens assez fort(e) pour relever le défi du Watch Club ?
        <br>
            <p>Avant tout, tu dois connaitre les regles qui régissent ce jeu :</p>
            <br>
            <br>
        <ul>
            <li>I : On ne parle PAS du club.</li>
            <br>
            <li>II : Attention, tu as deux minutes pour répondre à toutes les questions..</li>
            <br>
            <li>III : Tu devras répondre à TOUTES les questions pour terminer le questionnaire.</li>
            <br>
            <li>IV : On ne parle SURTOUT pas du club.</li>
            <br>
            <li>V : Les questions sont à choix unique.</li>
            <br>
            <li>VI : Du plaisir, tu prendras.</li>
            <br>
            <li>VII : A la fin de chaque quiz, tu pourras consulter ton classement.</li>
            <br>
            <li>VIII : Tu ne dois absolument pas parler du club à ton CHAT.</li>
            <br>
            <li>IX : Tu peux tenter ta chance autant de fois que tu le souhaites.</li>
            <br>
            <li>X : Bon, tu peux parler du club si tu veux...</li>
            <br>
        </ul>
        <br>
        <p>Tu es pret(e) Alors ça commence juste en dessous</p>
        <button onclick="window.location='thema.php'">Go</button>
        <button><a href="./deconnexion.php">Deconnexion</a></button>
    </section>

    <footer>
        <img src="./logo.png" alt="logo-footer">
        <p>Copyright 2021</p>
        <p>Ferrara Julien</p>
    </footer>
    
    
</body>
</html>