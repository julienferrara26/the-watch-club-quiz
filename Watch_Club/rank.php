<?php

session_start();

$theme = $_GET['id_theme'];

        // PDO

        include('./pdo.php');
          
        // Requete pour trier les données

          $sql1 = "SELECT Utilisateurs.Name as Name,
          user_score.score as Score
          FROM user_score
          JOIN Utilisateurs ON user_score.id_user = Utilisateurs.id
          WHERE user_score.id_theme = :theme
          ORDER BY Score DESC
          LIMIT 10 ";
          $requetePrep1 = $dbco->prepare($sql1);
          $requetePrep1->bindParam(':theme', $theme);
          $requetePrep1->execute();

        // Récupérer les données de la base
          
           $data = $requetePrep1->fetchAll();
