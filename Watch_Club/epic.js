class Question {
  constructor(text, choices, answer) {
    this.text = text;
    this.choices = choices;
    this.answer = answer;
  }
  isCorrectAnswer(choice) {
    return this.answer === choice;
  }
}

let questions = [
  new Question("Quel acteur joue Boromir dans la “La Communauté de l'Anneau” ?", ["Viggo Mortensen", "Orlando Bloom", "Sean Bean", "Ian Mckellen"], "Sean Bean"),
  new Question("Contre quelle nation se bat Mel Gibson dans le film “Braveheart” ?", ["Angleterre","France", "Ecosse", "Irlande"], "Angleterre"),
  new Question("Quel est le nom du personnage principal de la série “The Witcher” ?", ["Kratos","Geralt De Riv", "Coco l'asticot", "Ragnar De Divebois"], "Geralt De Riv"),
  new Question("Dans le “Seigneur des Anneaux” 20 anneaux de pouvoirs ont été forgés. Combien ont été donné aux Hommes ?", ["7","1", "9", "3"], "9"),
  new Question("Dans “Game of Thrones” a quelle Maison appartient Daenerys ?", ["Stark","Lannister", "Targaryen", "Baratheon"], "Targaryen"),
  new Question("Dans le film “Ran” combien de fils a Hidetora Ichimonji ?", ["2","6", "3", "1"], "3"),
  new Question("Comment s'appelle le nom de l'empereur que combat Maximus dans le film “Gladiator” ?", ["Commode","Marc Aurèle", "Jules Cesar", "Buffet"], "Commode"),
  new Question("Quel actrice joue le rôle de Jeanne d'Arc dans le film du même nom réalisé en 1999 par Luc Besson ?", ["Anne Parillaud","Milla Jovovich", "Sienna Miller", "Maiwenn"], "Milla Jovovich"),
  new Question("Dans le film “300” de Zack Snyder comment se nomme la bataille durant laquelle Spartiates et Perses s'affrontent ?", ["La bataille des Batards ","La bataille des Champs du Pelennor", "La bataille des Thermopyles", "La bataille Stirling"], "La bataille des Thermopyles"),
  new Question("Quel est le prénom de la femme de Ragnar Lothbrok dans la série “Vikings” ?", ["Melinda","Arya", "Laherta", "Lagertha"], "Lagertha")
];

class Quiz {
  constructor(questions) {
    this.score = 0;
    this.questions = questions;
    this.currentQuestionIndex = 0;
  }
  getCurrentQuestion() {
    return this.questions[this.currentQuestionIndex];
  }
  guess(answer) {
    if (this.getCurrentQuestion().isCorrectAnswer(answer)) {
      this.score++;
    }
    this.currentQuestionIndex++;
  }
  hasEnded() {
    return this.currentQuestionIndex >= this.questions.length;
  }
}


////// REGROUPER TOUTES LES FONCTIONS RELATIVES A L'AFFICHAGE (comme ça c'est plus propre tiavuuu)

const display = {
  elementVisible: function(id, text) {
    let element = document.getElementById(id);
    element.innerHTML = text;
  },
  endQuiz: function() {
    endQuizHTML = `
    <h1>Quiz terminé !</h1>
    <h3> Ton score est de : ${quiz.score} / ${quiz.questions.length}</h3>
    <h4><a href="./classement.php?id_theme=3">Ton classement</a></h4>`;
    this.elementVisible("quiz", endQuizHTML);
  },
  score: function() {
    return quiz.score
  },
  question: function() {
    this.elementVisible("question", quiz.getCurrentQuestion().text);
  },
  choices: function() {
    let choices = quiz.getCurrentQuestion().choices;
    
    guessHandler = (id, guess) => {
      document.getElementById(id).onclick = function() {
        quiz.guess(guess);
        quizApp();
      }
    }
    
    // AFFICHAGE DES CHOIX ET DE LA PROGRESSION
    
    for(let i = 0; i < choices.length; i++) {
      this.elementVisible("choice" + i, choices[i]);
      guessHandler("guess" + i, choices[i]);
    }
  },
  progress: function() {
    let currentQuestionNumber = quiz.currentQuestionIndex + 1;
    this.elementVisible("progress", "Question " + currentQuestionNumber + " sur " + quiz.questions.length);
    this.elementVisible("score", "Score :" + quiz.score);
  },
};

// Logique du jeu

quizApp = () => {
  if (quiz.hasEnded()) {
    display.endQuiz();
    let SCOREDATA = {
      score: display.score(),
      theme: 3
    }
    sendScores(SCOREDATA)
  } else {
    display.question();
    display.choices();
    display.progress();
  } 
}

// Creation Quiz

let quiz = new Quiz(questions);
quizApp();

console.log(quiz);

// Compte à rebours

    let startingMinutes = 2; // A Combien de temps on commence

    let time = startingMinutes * 60;
  
    const countDownEl = document.getElementById('countdown');
  
    setInterval(updateCountdown, 1000); // J'appelle la fonction toutes les secondes
  
    function updateCountdown() {
      const minutes = Math.floor(time / 60); 
      let seconds = time % 60;
  
      seconds = seconds < 10 ? '0' + seconds : seconds; // Je concatene un 0
  
      countDownEl.innerHTML = `${minutes} : ${seconds}`; // On affiche et des que le chrono se finit, on arrete le quiz
      if (time > 0) {
      time--; 
      } else {
          display.endQuiz();
          let SCOREDATA = {
            score: display.score(),
        }
          sendScores(SCOREDATA)
      }
    }

// Envoi score

function sendScores (SCOREDATA) {
  
  fetch('./uploadScore.php', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/JSON',
    },
    body: JSON.stringify(SCOREDATA),
  })
}
