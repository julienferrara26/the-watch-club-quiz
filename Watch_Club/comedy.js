class Question {
    constructor(text, choices, answer) {
      this.text = text;
      this.choices = choices;
      this.answer = answer;
    }
    isCorrectAnswer(choice) {
      return this.answer === choice;
    }
  }

  let questions = [
    new Question("Dans “Asterix & Obelix : Mission Cleopatre” que doit construire le personnage de Numerobis ?", ["Une pyramide", "Un casino", "Un palais", "Un cerceuil"], "Un palais"),
    new Question("De quelle mythologie le film “Monthy Python, Sacré Graal” s'inspire t'il ?", ["Grecque", "Egyptienne", "Arturienne", "Nordique"], "Arturienne"),
    new Question("Comment se nomme le personnage interpreté par Cameron Diaz dans “Mary à tout prix” ?", ["Mary", "Jeanne", "Daisy", "Michel"], "Mary"),
    new Question("Dans la série “Kaamelott”, Léodagan se fait surnommer ?", ["Le cruel", "Le magnanime", "Le sanguinaire", "Le juste"], "Le sanguinaire"),
    new Question("Dans la série “H” les protagonistes font partie d'un ?", ["Hopital", "Magasin", "Ranch", "Vaisseau"], "Hopital"),
    new Question("Louis de Funès n'a pas joué dans ?", ["La soupe au choux", "Le clan des Siciliens", "Le gendarme à St Tropez", "Fantomas"], "Le clan des Siciliens"),
    new Question("Completez cette replique de OSS 117 : “J'aime me beurrer la...” ?", ["Jambe", "Biscotte", "Panse", "Tartine"], "Biscotte"),
    new Question("Dans “Very Bad Trip” ou se trouve le futur marié que les trois comparses cherchent désespérément ?", ["Dans un trou", "Au bar", "A la morgue", "Sur le toit du casino"], "Sur le toit du casino"),
    new Question("Phoebe Waller-Bridge est l'interprete d'une série qui remporta en 2020 le Golden Globe de la meilleure série comique, le nom de cette série est ?", ["Mouche", "Fleabag", "Girls", "The L World"], "Fleabag"),
    new Question("Dans la serie “Kaamelott” quelle est la botte secrète de Perceval ?", ["Sloubi", "Pays de Galles Indépendannnnnnnnnt", "Remonte ton slibard Lothar", "C'est pas faux"], "C'est pas faux")
  ];

  console.log(Question);

  class Quiz {
    constructor(questions) {
      this.score = 0;
      this.questions = questions;
      this.currentQuestionIndex = 0;
    }
    getCurrentQuestion() {
      return this.questions[this.currentQuestionIndex];
    }
    guess(answer) {
      if (this.getCurrentQuestion().isCorrectAnswer(answer)) {
        this.score++;

      }
      this.currentQuestionIndex++;
    }
    hasEnded() {
      return this.currentQuestionIndex >= this.questions.length;
    }
  }


////// REGROUPER TOUTES LES FONCTIONS RELATIVES A L'AFFICHAGE (comme ça c'est plus propre tiavuuu)

const display = {
    elementVisible: function(id, text) {
      let element = document.getElementById(id);
      element.innerHTML = text;
    },
    endQuiz: function() {
      endQuizHTML = `
        <h1>Quiz terminé !</h1>
        <h3> Ton score est de : ${quiz.score} / ${quiz.questions.length}</h3>
        <h4><a href="./classement.php?id_theme=2">Ton classement</a></h4>`;
      this.elementVisible("quiz", endQuizHTML);
    },
    score: function() {
      return quiz.score
    },
    question: function() {
      this.elementVisible("question", quiz.getCurrentQuestion().text);
    },
    choices: function() {
      let choices = quiz.getCurrentQuestion().choices;
  
      guessHandler = (id, guess) => {
        document.getElementById(id).onclick = function() {
          quiz.guess(guess);
          quizApp();
        }
      }

          // AFFICHAGE DES CHOIX ET DE LA PROGRESSION

    for(let i = 0; i < choices.length; i++) {
        this.elementVisible("choice" + i, choices[i]);
        guessHandler("guess" + i, choices[i]);
      }
    },
    progress: function() {
      let currentQuestionNumber = quiz.currentQuestionIndex + 1;
      this.elementVisible("progress", "Question " + currentQuestionNumber + " sur " + quiz.questions.length);
      this.elementVisible("score", "Score : " + quiz.score);
    },
  };

  // Logique du jeu

quizApp = () => {
    if (quiz.hasEnded()) {
      display.endQuiz();
      let SCOREDATA = {
        score: display.score(),
        theme: 2
      }
      sendScores(SCOREDATA)
    } else {
      display.question();
      display.choices();
      display.progress();
    } 
  }

  // Creation Quiz

  let quiz = new Quiz(questions);
  quizApp();
  
  console.log(quiz);

  // Compte à rebours

    let startingMinutes = 2; // A Combien de temps on commence

    let time = startingMinutes * 60;
  
    const countDownEl = document.getElementById('countdown');
  
    setInterval(updateCountdown, 1000); // J'appelle la fonction toutes les secondes
  
    function updateCountdown() {
      const minutes = Math.floor(time / 60); 
      let seconds = time % 60;
  
      seconds = seconds < 10 ? '0' + seconds : seconds; // Je concatene un 0
  
      countDownEl.innerHTML = `${minutes} : ${seconds}`; // On affiche et des que le chrono se finit, on arrete le quiz
      if (time > 0) {
      time--; 
      } else {
          display.endQuiz();
          let SCOREDATA = {
            score: display.score(),
        }
          sendScores(SCOREDATA)
      }
    }

  // Envoi score

  function sendScores (SCOREDATA) {

    fetch('./uploadScore.php', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/JSON',
      },
      body: JSON.stringify(SCOREDATA),
    })
  }