<?php

////Ouvrir la session 

session_start();

include('./rank.php');

$name = $_SESSION['Name'];

if (empty($_SESSION['Name']))
    {
        header('location:erreur403b.php?');;
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="classement.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300&display=swap" rel="stylesheet">
    <title>Classement</title>
</head>
<body>

    <header>
        <img src="./The watch.png" alt="logo">
    </header>

    <h1><?= $_SESSION['Name'] ?></h1>
        <br>
    <section>
            <h2>Classement</h2>
            <br>
            <br>
            <table>
            
                    <tr>
                    <th>Nom</th>
                    <th>Score</th>
                    </tr>
                    
            <?php foreach($data as $data): ?>

                        <tr>
                        <td> 
                            <?= $data ['Name']?> 
                        </td>
                        <td>
                            <?= $data ['Score'] ?>
                        </td>
                        </tr>

            <?php endforeach; ?>
                
            </table>
        <br>
        <button onclick="window.location='thema.php'">Continuer ?</button>
        <button><a href="./deconnexion.php">Deconnexion</a></button>
    </section>

    <footer>
        <img src="./logo.png" alt="logo-footer">
        <p>Copyright 2021</p>
        <p>Ferrara Julien</p>
    </footer>
    
    
</body>
</html>