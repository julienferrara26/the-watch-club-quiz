<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="erreur.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300&display=swap" rel="stylesheet">
    <title>Inscription</title>
</head>
<body>

    <header>
        <img src="./The watch.png" alt="logo">
    </header>

    <section class="Utilisateur">
        <p>Je crois que tu n'es pas connecté(e)...</p>
        <br>
        <p>Retentes ta chance par <a href="vitrine.html">là</a></p>
        <div id="logo"><img src="./logo.png" alt="logo"></div>
    </section>

    <footer>
        <img src="./logo.png" alt="logo-footer">
        <p>Copyright 2021</p>
        <p>Ferrara Julien</p>
    </footer>
    
</body>
</html>