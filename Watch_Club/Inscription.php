<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="accueil.css">
    <script src="accueil.js" async></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300&display=swap" rel="stylesheet">
    <title>Inscription</title>
</head>
<body>

    <header>
        <img src="./The watch.png" alt="logo">
    </header>

    <section class="Utilisateur">
        <p>Viens tester tes connaissances sur le 7eme art et deviens le maitre incontesté du Club.</p>
        <br>
        <p>Premiere chose, entres ton blaze et suis le lapin blanc.</p>
        <p>Tu fais déjà partie du club ? Alors cliques juste <a href="connexion.php">ici</a></p>

        <form action="traitement_inscript.php" method="POST">
            <div><input type="text" id="name" name="name" placeholder="Nom" /></div>
            <input type="email" name="email" id="email" placeholder="Mail">
            <input type="password" name="password" id="password" placeholder="Password"/>
            <div>
              <select name="pill" id="pill">
                <option value="">Choisissez une Pillule</option>
                <option value="Bleue">Pillule Bleue</option>
                <option value="Rouge">Pillule Rouge</option>
              </select>
            </div>
            <input type="submit" value="Valider"> 
        </form>
          
        <div id="resultat">
            Nom : <span id="res-name"></span><br />
            Email : <span id="res-email"></span><br />
            Pillule : <span id="res-pill"></span><br />
        </div>
    </section>

    <footer>
        <img src="./logo.png" alt="logo-footer">
        <p>Copyright 2021</p>
        <p>Ferrara Julien</p>
    </footer>
    
</body>
</html>