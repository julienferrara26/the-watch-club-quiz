///////////classe constructeur d'objets//////////

class Question {
    constructor(text, choices, answer) {
      this.text = text;
      this.choices = choices;
      this.answer = answer;
    }

    ////Fonction booleenne////

    isCorrectAnswer(choice) {
      return this.answer === choice;
    }
  }

/////Tableau des questions////
////Chacun de ses objets contient la fonction isCorrectAnswer////

  let questions = [
    new Question("Qui a réalisé “Heat” sorti en 1995 ?", ["Martin Scorsese", "Quentin Tarrantino", "Michael Mann", "Guillaume Canet"], "Michael Mann"),
    new Question("Qui a reçu le plus grand nombre d'Oscars dans la catégorie Meilleur Acteur ?", ["Daniel Day Lewis","Robert De Niro", "Clint Eastwood", "Christian Clavier"], "Daniel Day Lewis"),
    new Question("Quelle actrice tient l'affiche du film Titanic en 1998 ?", ["Julia Roberts","Josianne Balasko", "Kate Winslet", "Jennifer Anniston"], "Kate Winslet"),
    new Question("Combien de suites le film Fast & Furious contient-il ?", ["7","124", "9", "10"], "9"),
    new Question("Quel est le film le plus rentable du Cinéma au Box Office ?", ["Titanic","Avatar", "Bienvenue chez les Ch'tis", "Le Roi Lion"], "Avatar"),
    new Question("Quelle actrice a remporté le César de la Meilleure Actrice en 2010 pour son role dans “La Journée de la jupe” ?", ["Audrey Tautou","Emmanuelle Béart", "Isabelle Adjani", "Catherine Deneuve"], "Isabelle Adjani"),
    new Question("Dans quel série peut on voir le personnage de Saul Goodman ?", ["The Shield","Les Soprano", "Breaking Bad", "Joséphine Ange Gardien"], "Breaking Bad"),
    new Question("Sur quelle planète se situe l'action du film Dune ?", ["Arrakis","Naboo", "Titan", "Mars"], "Arrakis"),
    new Question("Où Frodon et Sam doivent-ils jeter l'Anneau de Pouvoir ?", ["Fondcombes","La Moria", "Le Mordor", "Dans un trou"], "Le Mordor"),
    new Question("Quelle actrice joue le role de Edith Piaf dans le film “La Mome” ?", ["Gerard Depardieu","Marion Cotillard", "Audrey Fleurot", "Emmanuelle Devos"], "Marion Cotillard")
  ];


  class Quiz {
    constructor(questions) {
      this.score = 0;
      this.questions = questions;
      this.currentQuestionIndex = 0;
    } 
    getCurrentQuestion() {
      return this.questions[this.currentQuestionIndex];
    }
    guess(answer) {
      if (this.getCurrentQuestion().isCorrectAnswer(answer)) {
        this.score++;
      }
      this.currentQuestionIndex++;
    }
    hasEnded() {
      return this.currentQuestionIndex >= this.questions.length;
    }
  }

////// REGROUPER TOUTES LES FONCTIONS RELATIVES A L'AFFICHAGE (comme ça c'est plus propre)

const display = {
    elementVisible: function(id, text) {
      let element = document.getElementById(id);
      element.innerHTML = text;
    },
    endQuiz: function() {
      endQuizHTML = `
        <h1>Quiz terminé !</h1>
        <h3> Ton score est de : ${quiz.score} / ${quiz.questions.length}</h3>
        <h4><a href="./classement.php?id_theme=5">Ton classement</a></h4>`;
      this.elementVisible("quiz", endQuizHTML);
    },
    score: function() {
      return quiz.score
    },
    question: function() {
      this.elementVisible("question", quiz.getCurrentQuestion().text);
    },
    choices: function() {
      let choices = quiz.getCurrentQuestion().choices;

    guessHandler = (id, guess) => {
        document.getElementById(id).onclick = function() {
          quiz.guess(guess);
          quizApp();
        }
      }

          // AFFICHAGE DES CHOIX ET DE LA PROGRESSION

    for(let i = 0; i < choices.length; i++) {
        this.elementVisible("choice" + i, choices[i]);
        guessHandler("guess" + i, choices[i]);
      }
    },
    progress: function() {
      let currentQuestionNumber = quiz.currentQuestionIndex + 1;
      this.elementVisible("progress", "Question " + currentQuestionNumber + " sur " + quiz.questions.length);
      this.elementVisible("score", "Score : " + quiz.score);
    },
  };

  // Logique du jeu

quizApp = () => {
    if (quiz.hasEnded()) {
      ///FINI///
      display.endQuiz();
      let SCOREDATA = {
        score: display.score(),
        theme: 5
      }
      sendScores(SCOREDATA)
    } else {
      display.question();
      display.choices();
      display.progress();
    } 
  }

  // Creation Quiz

  let quiz = new Quiz(questions);
  quizApp();
  
  console.log(quiz);

  // Compte à rebours

  let startingMinutes = 2; // A Combien de temps on commence

  let time = startingMinutes * 60;

    const countDownEl = document.getElementById('countdown');

    setInterval(updateCountdown, 1000); // J'appelle la fonction toutes les secondes

    function updateCountdown() {
      const minutes = Math.floor(time / 60); 
      let seconds = time % 60;

      seconds = seconds < 10 ? '0' + seconds : seconds; // Je concatene un 0

      countDownEl.innerHTML = `${minutes} : ${seconds}`; // On affiche et des que le chrono se finit, on arrete le quiz
      if (time > 0) {
      time--; 
      } else {
        display.endQuiz();
        let SCOREDATA = {
          score: display.score(),
        }
        sendScores(SCOREDATA)
      }
    }
  
  // Envoi score

  function sendScores (SCOREDATA) {

    fetch('./uploadScore.php', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/JSON',
      },
      body: JSON.stringify(SCOREDATA),
    })
  }