
 
-- On créé la base --

create DATABASE IF NOT EXISTS The_Watch Club; 

-- Puis on se positionne dessus --

use The_watch_Club;

-- Creation super utilisateur 

create user 'The_Watcher'@'localhost' identified by 'Woody012016!';

-- On lui donne les droits 

grant all on The_Watch_Club.* TO 'The_Watcher'@'localhost' identified by 'Woody012016!';


-- Puis on crée les tables --

create table Utilisateurs (
    Id INT UNSIGNED NOT NULL;
    Name VARCHAR(30) NOT NULL,
    Pillule CHAR(5) NOT NULL,
    Password VARCHAR(300) NOT NULL,
    Mail VARCHAR(50) NOT NULL
    PRIMARY KEY (Id)
);

create table Themes (
    id_theme INT UNSIGNED NOT NULL,
    name_theme INT UNSIGNED NOT NULL,
    PRIMARY KEY(id_theme)
);

create table user_score (
    id_user INT UNSIGNED NOT NULL,
    id_theme INT UNSIGNED NOT NULL,
    score INT UNSIGNED,
    PRIMARY KEY(id_user, id_theme),

    CONSTRAINT fk_usertheme
    FOREIGN KEY (id_theme)
    REFERENCES themes(id_theme),

    CONSTRAINT fk_userscore
    FOREIGN KEY (id_user)
    REFERENCES Utilisateurs(Id)
);

create table Questions (
    id INT UNSIGNED NOT NULL,
    Questions TEXT NOT NULL,
    id_theme INT UNSIGNED NOT NULL,
    PRIMARY KEY (id),

    CONSTRAINT fk_question
    FOREIGN KEY (id_theme)
    REFERENCES Themes(id_theme)
);

create table Reponses (
    id INT UNSIGNED NOT NULL,
    id_Questions INT UNSIGNED NOT NULL,
    Texte VARCHAR (250) NOT NULL,
    bonnes_rep TINYINT NOT NULL,
    PRIMARY KEY(Id),

    CONSTRAINT fk_reponses
    FOREIGN KEY (id_Questions)
    REFERENCES Questions(Id)
);


--Supression

drop table Themes;

-- Modifications

ALTER TABLE Questions ADD CONSTRAINT fk_question FOREIGN KEY (Id_Theme) REFERENCES Themes(Id_Themes);

ALTER TABLE Themes MODIFY name_theme VARCHAR(50) NOT NULL;

/// Insertion des themes ////

insert into Themes
    VALUES (1, 'Animation'),
    (2, 'Comedie'),
    (3, 'Epic'),
    (4, 'Gangster'),
    (5, 'General'),
    (6, 'SF');



