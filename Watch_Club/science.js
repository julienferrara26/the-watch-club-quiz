class Question {
    constructor(text, choices, answer) {
      this.text = text;
      this.choices = choices;
      this.answer = answer;
    }
    isCorrectAnswer(choice) {
      return this.answer === choice;
    }
  }

  let questions = [
    new Question("Comment se nomme les androides que doit traquer Harrison Ford dans “Blade Runner” ?", ["Les Machines", "Les Replicants", "Les Moutons", "Les Vipères"], "Les Replicants"),
    new Question("Quel est le nom du vaisseau dans “Alien le 8ème passager” ?", ["Komodo", "Totoro", "Nostromo", "Pepito"], "Nostromo"),
    new Question("Qui réalise le film “2001 : l'odyssée de l'espace” ?", ["Stanley Kubrick", "James Cameron", "Ridley Scott", "Orson Welles"], "Stanley Kubrick"),
    new Question("Quelle actrice interprete Irene Cassini dans “Bienvenue à Gattaca” ?", ["Julianne Moore", "Kim Bassinger", "Uma Thurman", "Gwyneth Paltrow"], "Uma Thurman"),
    new Question("Dans le film “Matrix” Carrie Ann Moss interprete le personnage de ?", ["Velocity", "Trinity", "Sissi", "Spicy"], "Trinity"),
    new Question("Dans le film “Terminator 2” Arnold Schwarrzenegger se frite avec un ?", ["T-800", "T-900", "T-1000", "T-1200"], "T-1000"),
    new Question("Contre qui les habitants de la Terre se battent ils dans “Mars Attack” ?", ["Des vaches", "Des plantes", "Des aligators", "Des extra-terrestres"], "Des extra-terrestres"),
    new Question("Comment s'appelle le petit garçon qui receuille “E.T l'extra-terrestre” ?", ["Eliott", "Liam", "Josh", "Tommy"], "Eliott"),
    new Question("Le personnage de Furiosa interpreté par Charlize Theron se trouve dans quel film ?", ["Aliens", "Starship Troopers", "Mad Max - Fury Road", "The Thing"], "Mad Max - Fury Road"),
    new Question("Quel réalisateur est aux commandes de “L'empire contre-attaque” ?", ["Steven Spielberg", "George Lucas", "Irvin Kershner", "JJ Abrams"], "Irvin Kershner")
  ];

  class Quiz {
    constructor(questions) {
      this.score = 0;
      this.questions = questions;
      this.currentQuestionIndex = 0;
    }
    getCurrentQuestion() {
      return this.questions[this.currentQuestionIndex];
    }
    guess(answer) {
      if (this.getCurrentQuestion().isCorrectAnswer(answer)) {
        this.score++;
        
      }
      this.currentQuestionIndex++;
    }
    hasEnded() {
      return this.currentQuestionIndex >= this.questions.length;
    }
  }


////// REGROUPER TOUTES LES FONCTIONS RELATIVES A L'AFFICHAGE (comme ça c'est plus propre tiavuuu)

const display = {
    elementVisible: function(id, text) {
      let element = document.getElementById(id);
      element.innerHTML = text;
    },
    endQuiz: function() {
      endQuizHTML = `
        <h1>Quiz terminé !</h1>
        <h3> Ton score est de : ${quiz.score} / ${quiz.questions.length}</h3>
        <h4><a href="./classement.php?id_theme=6">Ton classement</a></h4>`;
      this.elementVisible("quiz", endQuizHTML);
    },
    score: function() {
      return quiz.score
    },
    question: function() {
      this.elementVisible("question", quiz.getCurrentQuestion().text);
    },
    choices: function() {
      let choices = quiz.getCurrentQuestion().choices;
  
      guessHandler = (id, guess) => {
        document.getElementById(id).onclick = function() {
          quiz.guess(guess);
          quizApp();
        }
      }

          // AFFICHAGE DES CHOIX ET DE LA PROGRESSION

    for(let i = 0; i < choices.length; i++) {
        this.elementVisible("choice" + i, choices[i]);
        guessHandler("guess" + i, choices[i]);
      }
    },
    progress: function() {
      let currentQuestionNumber = quiz.currentQuestionIndex + 1;
      this.elementVisible("progress", "Question " + currentQuestionNumber + " sur " + quiz.questions.length);
      this.elementVisible("score", "Score : " + quiz.score);
    },
  };

  // Logique du jeu

quizApp = () => {
    if (quiz.hasEnded()) {
      display.endQuiz();
      let SCOREDATA = {
        score: display.score(),
        theme: 6
      }
      sendScores(SCOREDATA)
    } else {
      display.question();
      display.choices();
      display.progress();
    } 
  }

  // Creation Quiz

  let quiz = new Quiz(questions);
  quizApp();
  
  console.log(quiz);

  // Compte à rebours

    let startingMinutes = 2; // A Combien de temps on commence

    let time = startingMinutes * 60;
  
    const countDownEl = document.getElementById('countdown');
  
    setInterval(updateCountdown, 1000); // J'appelle la fonction toutes les secondes
  
    function updateCountdown() {
      const minutes = Math.floor(time / 60); 
      let seconds = time % 60;
  
      seconds = seconds < 10 ? '0' + seconds : seconds; // Je concatene un 0
  
      countDownEl.innerHTML = `${minutes} : ${seconds}`; // On affiche et des que le chrono se finit, on arrete le quiz
      if (time > 0) {
      time--; 
      } else {
          display.endQuiz();
          let SCOREDATA = {
            score: display.score(),
        }
          sendScores(SCOREDATA)
      }
    }

  // Envoi score 
  
  function sendScores (SCOREDATA) {

    fetch('./uploadScore.php', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/JSON',
      },
      body: JSON.stringify(SCOREDATA),
    })
  }
