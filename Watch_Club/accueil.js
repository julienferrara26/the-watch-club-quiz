document
  .getElementById("name")
  .addEventListener("input", function(e) {
    document
      .getElementById("res-name")
      .innerText = e.target.value;
});

document
  .getElementById("pill")
  .addEventListener("change", function(e) {
    document
      .getElementById("res-pill")
      .innerText = e.target.value;
});

document
  .getElementById("email")
  .addEventListener("change", function(e) {
    document
      .getElementById("res-email")
      .innerText = e.target.value;
});
