<?php

session_start();
$nom = $_SESSION['Name'];
$data = trim(file_get_contents('php://input'));
$decoded = json_decode($data, true);
$score = (int) $decoded['score'];
$theme = (int) $decoded['theme'];

include('./pdo.php');

/// Requete preparée pour recuperer le score

$sql = "SELECT *  FROM user_score 
    WHERE id_theme = :theme
    AND id_user = (select Id from Utilisateurs where Name = :Name
    )";

    $requetePrep1 = $dbco->prepare($sql);
    $requetePrep1->bindParam(':Name', $nom);
    $requetePrep1->bindParam(':theme', $theme);
    $requetePrep1->execute();
    $data = $requetePrep1->fetch();

/// On compare les scores pour écraser l'ancien si celui est inferieur au nouveau 

if($data) {
    $oldscore = (int) $data ['score'];
    if ($oldscore < $score) {
        $sql = "UPDATE user_score SET score = :score 
            WHERE id_user = (select Id from Utilisateurs where Name = :Name)
            AND id_theme = :theme
        ";
        $requetePrep1 = $dbco->prepare($sql);
        $requetePrep1->bindParam(':Name', $nom);
        $requetePrep1->bindParam(':score', $score);
        $requetePrep1->bindParam(':theme', $theme);
        $requetePrep1->execute();
    }
}
else {
    $sql = "INSERT INTO user_score(id_user, id_theme, score) 
        VALUES (
        (select Id from Utilisateurs where Name = :Name),
        :theme,
        :score
    )";
    $requetePrep1 = $dbco->prepare($sql);
    $requetePrep1->bindParam(':Name', $nom);
    $requetePrep1->bindParam(':score', $score);
    $requetePrep1->bindParam(':theme', $theme);
    $requetePrep1->execute();
}

